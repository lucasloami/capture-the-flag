#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>
#include <errno.h>

#define NROBOTS 3
#define MATRIXSIZE 10
#define CELLVALUE 0
#define ROBOTVALUE 1
#define FLAGVALUE 2
#define ENDPOINTVALUE 3
#define EARTHQUAKEVALUE 4

typedef struct robot_struct {
  int id;
  int row;
  int column;
  int previousCellValue;
  int haveFlag;
}robot_t;

typedef struct target_struct {
  int row;
  int column;
}target_t;

robot_t robots[NROBOTS];
target_t flag, endPoint, earthquake;
int field[MATRIXSIZE][MATRIXSIZE];
pthread_mutex_t lockRobot[MATRIXSIZE*MATRIXSIZE];
pthread_mutex_t lockPrint;
// pthread_mutex_t lockOps;
pthread_barrier_t barrier;
int winner, someoneHasFlag;

void usleep(int miliseconds);

void nsleep(int miliseconds) {
  usleep(1000 * miliseconds);
}

void initField() {
  int i, j;

  for (i = 0; i < MATRIXSIZE; i++)
  {
    for (j = 0; j < MATRIXSIZE; j++)
    {
      field[i][j] = CELLVALUE;
    }
  }
}

void initRobots() {
  int r1, r2, i;

  i = 0;
  while(i < NROBOTS)
  {
    r1 = rand() % MATRIXSIZE; 
    r2 = rand() % MATRIXSIZE;
    if (field[r1][r2] == 0) // se a celula ta vazia
    {
      // Define posicao do robo
      robots[i].id = i;
      robots[i].haveFlag = 0;
      robots[i].previousCellValue = CELLVALUE;
      robots[i].row = r1;
      robots[i].column = r2;

      // Coloca o robo no field
      field[r1][r2] = ROBOTVALUE;

      // pthread_mutex_lock(&lockRobot[robots[i].row * MATRIXSIZE + robots[i].column]);

      i++;
    }
  }
}

void initFlag() {
  int r1, r2, i;

  i = 0;
  while(i < 1)
  {
    r1 = rand() % MATRIXSIZE; 
    r2 = rand() % MATRIXSIZE;
    if (field[r1][r2] == 0) // se a celula ta vazia
    {

      // Define posicao do robo
      flag.row = r1;
      flag.column = r2;

      // Coloca o robo no field
      field[r1][r2] = FLAGVALUE;
      i++;
    }
  }
}

void initEndPoint() {
  // int r1, r2, i;

  // Define posicao do endpoint
  endPoint.row = 0;
  endPoint.column = 0;
  // Coloca o endPoint no field
  field[0][0] = ENDPOINTVALUE;    

  // i = 0;
  // while(i < 1)
  // {
  //  r1 = rand() % MATRIXSIZE; 
  //    r2 = rand() % MATRIXSIZE;
  //  if (field[r1][r2] == 0) // se a celula ta vazia
  //  {

  //    // Define posicao do robo
  //    endPoint.row = r1;
  //    endPoint.column = r2;

  //      // Coloca o robo no field
  //      field[r1][r2] = 3;
  //    i++;
  //  }
  // }
}

void initEarthquake() {
  // Define posicao do endpoint
  earthquake.row = MATRIXSIZE-1;
  earthquake.column = MATRIXSIZE-1;

  // Coloca o earthquake no field
  field[MATRIXSIZE-1][MATRIXSIZE-1] = EARTHQUAKEVALUE;  
}

void initLocks() {
  int i;
  for (i = 0; i <= MATRIXSIZE*MATRIXSIZE; i++)
  {
    pthread_mutex_init(&lockRobot[i], NULL);
    // lockRobot[i] = PTHREAD_MUTEX_INITIALIZER;
  }
  pthread_mutex_init(&lockPrint, NULL);
  // pthread_mutex_init(&lockOps, NULL);
}

void init() {
  initLocks();
  initField();
  initEarthquake();
  initEndPoint(); 
  initFlag();
  initRobots();
  pthread_barrier_init(&barrier,NULL,NROBOTS);
  winner = 0;
  someoneHasFlag = -1;
}

void showField() {
  int i, j;
  for (i = 0; i < MATRIXSIZE; i++)
  {
    for (j = 0; j < MATRIXSIZE; j++)
    {
      if (field[i][j] == CELLVALUE)
      {
        printf(".    ");
      } else if (field[i][j] == ROBOTVALUE)
      {
        printf("R    ");
      } else if (field[i][j] == FLAGVALUE)
      {
        printf("F    ");
      } else if (field[i][j] == ENDPOINTVALUE)
      {
        printf("E    ");
      } else if (field[i][j] == EARTHQUAKEVALUE)
      {
        printf("T    ");
      }
    }
    printf("\n");
  }
}

void walk(int id, target_t target) {
  field[robots[id].row][robots[id].column] = robots[id].previousCellValue;
  int previousPositionLock = robots[id].row * MATRIXSIZE + robots[id].column;

  // testa se o local pra onde o robo vai caminhar ja esta ocupado
  // decide pra qual direcao o robo andara

  // printf("id=%d\n", id);
  // printf("target X=%d e Y=%d\n", target.row, target.column);
  // printf("robot X=%d e Y=%d\n", robots[id].row, robots[id].column);


  // pthread_mutex_lock(&lockRobot[positionToLock]);
  if (target.row - robots[id].row > 0 && 
    (pthread_mutex_trylock(&lockRobot[(robots[id].row + 1) * MATRIXSIZE + robots[id].column]) != EBUSY) ) 
  {
    // printf("lock=%d\n", (robots[id].row + 1) * MATRIXSIZE + robots[id].column);
    // printf("caminhei pra baixo\n");
    //caminha pra direita
    robots[id].row +=1;
    pthread_mutex_unlock(&lockRobot[previousPositionLock]);

  } else if (target.row - robots[id].row < 0 && 
    (pthread_mutex_trylock(&lockRobot[(robots[id].row - 1) * MATRIXSIZE + robots[id].column]) != EBUSY) ) 
  {
    // printf("lock=%d\n", (robots[id].row - 1) * MATRIXSIZE + robots[id].column);
    // printf("caminhei pra cima\n");
    //caminha pra esquerda
    robots[id].row -=1;
    pthread_mutex_unlock(&lockRobot[previousPositionLock]);

  } else if (target.column - robots[id].column > 0 && 
    (pthread_mutex_trylock(&lockRobot[(robots[id].row * MATRIXSIZE + robots[id].column) + 1]) != EBUSY) ) 
  {
    // printf("lock=%d\n", (robots[id].row * MATRIXSIZE + robots[id].column) + 1);
    // printf("caminhei pra direita\n");
    //caminha pra baixo
    robots[id].column +=1;
    pthread_mutex_unlock(&lockRobot[previousPositionLock]);    

  } else if (target.column - robots[id].column < 0 && 
    (pthread_mutex_trylock(&lockRobot[(robots[id].row * MATRIXSIZE + robots[id].column) - 1]) != EBUSY) ) 
  {
    // printf("lock=%d\n", (robots[id].row * MATRIXSIZE + robots[id].column) - 1);
    // printf("caminhei pra esquerda\n");
    //caminha pra cima
    robots[id].column -=1;
    pthread_mutex_unlock(&lockRobot[previousPositionLock]);    
  } else
  {
    // printf("nao entrou em nada\n");
  }

  // printf("--------------------\n");
  // pthread_mutex_unlock(&lockRobot[positionToLock]);

  // se a nova posicao eh a posicao da bandeira, entao zera a posicao para indicar que o robo pegou a bandeira
  if (field[robots[id].row][robots[id].column] == FLAGVALUE)
  {
    robots[id].previousCellValue = CELLVALUE;
  } else
  {
    robots[id].previousCellValue = field[robots[id].row][robots[id].column];
  }
  // fill cell
  field[robots[id].row][robots[id].column] = ROBOTVALUE;
}

void *search(void *arg) {
  int id = (*(int *) arg);
  while (!winner)
  {
    nsleep(125);

    if (robots[id].haveFlag)
    {
      walk(id, endPoint);
      flag.row = robots[id].row;
      flag.column = robots[id].column;
    } else if (someoneHasFlag >= 0)
    {
      walk(id, earthquake);
    } else
    {
      // printf("busco flag\n");
      walk(id, flag);
    }
    
    pthread_mutex_lock(&lockPrint);
    // se o robo nao tem a bandeira e esta na posicao da bandeira, pega a mesma
    if (!robots[id].haveFlag && robots[id].row == flag.row && robots[id].column == flag.column)
    {
      robots[id].haveFlag = 1;
      someoneHasFlag = id;
    
    // se o robo nao tem a banedira e esta na posicao do earthquake, faz tremer
    } else if (!robots[id].haveFlag && robots[id].row == earthquake.row && robots[id].column == earthquake.column)
    {
      robots[someoneHasFlag].haveFlag = 0;
      robots[someoneHasFlag].previousCellValue = CELLVALUE;
      someoneHasFlag = -1;
      initEarthquake();
      initFlag();
      // initRobots();
    // se o robo tem a bandeira e esta na saida, ele e o vencedor
    } else if (robots[id].haveFlag && robots[id].row == endPoint.row && robots[id].column == endPoint.column)
    {
      winner = 1;
    }
    // pthread_mutex_unlock(&lockOps);

    // pthread_mutex_lock(&lockPrint);
    system("clear");
    showField();
    pthread_mutex_unlock(&lockPrint);

    //COLOCAR UMA BARREIRA AQUI ANTES DE COMEÇAR A PRÓXIMA RODADA
    pthread_barrier_wait(&barrier);
  }
}


int main(int argc, char const *argv[])
{
  
  pthread_t robot_search[NROBOTS];
  int i, *id;

  srand ( time(NULL) );
  init();
  system("clear");
  showField();

  for (i = 0; i < NROBOTS; i++)
  {
    id = (int *) malloc(sizeof(int));
    *id = i;
    pthread_create(&robot_search[i], NULL, search, (void *) id);
  }

  pthread_join(robot_search[0],NULL);
  // pthread_join(robot_search[1],NULL);
  // pthread_join(robot_search[2],NULL);

  return 0;
}
