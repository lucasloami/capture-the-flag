 /*
    Criar um lock novo para evitar starvation nos escritores

 */


 #include "stdio.h"
 #include "unistd.h"
 #include "stdlib.h"
 #include "pthread.h"

 #define NUM_LEITOR 8 //numero de LEITORES
 #define NUM_ESCRITOR 4 //numero de ESCRITORES

pthread_mutex_t lbd = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t lrc = PTHREAD_MUTEX_INITIALIZER;
int rc = 0;
int valor_lido = 0;


void * escreve (void *arg) {
    int i = *((int *) arg);
	while (1) {
		//printf("produz o dado\n");
		sleep(3);
		//LOCK TURN
		pthread_mutex_lock(&lbd);
		printf("to escreveeeeeeendo dado %d\n", i+1);
		valor_lido = i+1;
		pthread_mutex_unlock(&lbd);
		//UNLOCK TURN
	}
}

void * le (void *arg) {
    int i = *((int *) arg);
	while(1) {
        sleep(3);
        //LOCK TURN
		pthread_mutex_lock(&lrc);
		rc += 1;
		if (rc == 1) pthread_mutex_lock(&lbd);
		pthread_mutex_unlock(&lrc);
		//UNLOCK TURN

		if(valor_lido == 0) printf("nao ha dados produzidos ainda\n");
        else printf("to leeeeeeeeeendo o dado : %d\n", valor_lido);

		pthread_mutex_lock(&lrc);
		rc -= 1;
		if (rc == 0) pthread_mutex_unlock(&lbd);
		pthread_mutex_unlock(&lrc);
		//printf("processando, processando, processaaaaaaaaaando\n");

	}
}

int main () {
	pthread_t leitores[NUM_LEITOR];
	pthread_t escritores[NUM_ESCRITOR];

	//printf("caguei");
	int i;
    int *id;

    for (i = 0; i < NUM_LEITOR ; i++) {
         id = (int *) malloc(sizeof(int));
         *id = i;
         pthread_create(&leitores[i], NULL, le, (void *) (id));
    }

    for (i = 0; i < NUM_ESCRITOR ; i++) {
        id = (int *) malloc(sizeof(int));
        *id = i;
        pthread_create(&escritores[i], NULL, escreve, (void *) (id));
    }


    pthread_join(escritores[0],NULL);
    pthread_join(leitores[0],NULL);

	return 0;
}


